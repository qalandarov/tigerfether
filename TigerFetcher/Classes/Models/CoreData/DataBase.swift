//
//  DataBase.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import CoreData

class DataBase {
    static let shared = DataBase()
    
    var context: NSManagedObjectContext?
    
    func save() {
        do {
            try context?.save()
        } catch let error {
            print("Error saving: \(error)")
        }
    }
}