//
//  Photo+CoreDataProperties.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Photo {

    @NSManaged var dateUpload: NSDate?
    @NSManaged var content: String?
    @NSManaged var farm: NSNumber?
    @NSManaged var imageURL: String // this will be built based on other fields and will never be nil
    @NSManaged var photoID: String?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var originalFormat: String?
    @NSManaged var originalSecret: String?
    @NSManaged var ownerName: String?
    @NSManaged var secret: String?
    @NSManaged var server: NSNumber?
    @NSManaged var tags: String?
    @NSManaged var title: String?
    @NSManaged var woeID: NSNumber?
    @NSManaged var place: Place?

}
