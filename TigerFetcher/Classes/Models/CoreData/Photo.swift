//
//  Photo.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import CoreData


class Photo: NSManagedObject {
    
    class func importPhotos(photos: [[String: AnyObject]]) -> [Photo]? {
        return photos.flatMap { return photo($0) } // convert and return non-nil Photo objects
    }
    
    // Get Photo from Dictionary (if exists by ID or create a new one)
    class func photo(photoDict: [String: AnyObject]) -> Photo? {
        
        if let photoID = photoDict[Constants.PhotoKey.PhotoID.rawValue] as? String
        {
            let photo = photoByID(photoID) ?? emptyPhoto()
            update(photo, withDictionary: photoDict)
            
            // If any of these is nil we can't get the image URL - no use of this object
            if photo.farm != nil || photo.secret != nil || photo.server != nil {
                return photo
            }
        }
        
        return nil
    }
    
    class func update(photo: Photo, withDictionary photoDict: [String: AnyObject]) {
        photo.photoID           = photoDict[Constants.PhotoKey.PhotoID.rawValue]        as? String
        photo.farm              = photoDict[Constants.PhotoKey.Farm.rawValue]           as? Int
        photo.latitude          = photoDict[Constants.PhotoKey.Latitude.rawValue]       as? Float
        photo.longitude         = photoDict[Constants.PhotoKey.Longitude.rawValue]      as? Float
        photo.originalFormat    = photoDict[Constants.PhotoKey.OriginalFormat.rawValue] as? String
        photo.originalSecret    = photoDict[Constants.PhotoKey.OriginalSecret.rawValue] as? String
        photo.ownerName         = photoDict[Constants.PhotoKey.OwnerName.rawValue]      as? String
        photo.secret            = photoDict[Constants.PhotoKey.Secret.rawValue]         as? String
        photo.server            = Int(photoDict[Constants.PhotoKey.Server.rawValue] as! String)
        photo.tags              = photoDict[Constants.PhotoKey.Tags.rawValue]           as? String
        photo.title             = photoDict[Constants.PhotoKey.Title.rawValue]          as? String
        photo.woeID             = photoDict[Constants.PhotoKey.WOEID.rawValue]          as? Int
        
        photo.content           = contentFromDict(photoDict)
        photo.dateUpload        = dateFromDict(photoDict)
        photo.imageURL          = Flickr.URL.ImageURL(photo: photo).path
        
        photo.place             = Place.placeByID(photoDict[Constants.PhotoKey.PlaceID.rawValue] as! String)
    }
    
    // MARK: - Helpers
    
    class func photoByID(photoID: String) -> Photo? {
        assert(photoID.characters.count > 0, "PhotoID should not be empty!")
        
        let request = NSFetchRequest(entityName: "Photo")
        request.predicate = NSPredicate(format: "photoID = %@", photoID)
        
        var photo : Photo?
        
        do {
            try photo = DataBase.shared.context?.executeFetchRequest(request).first as? Photo
        } catch {
            print("Error executing a fetch request: \(error)")
        }
        
        return photo
    }
        
    class func emptyPhoto() -> Photo {
        return NSEntityDescription.insertNewObjectForEntityForName("Photo", inManagedObjectContext: DataBase.shared.context!) as! Photo
    }

}

private extension Photo {
    class func contentFromDict(photoDict: [String: AnyObject]) -> String? {
        let components = Constants.PhotoKey.Content.rawValue.componentsSeparatedByString(".")
        assert(components.count == 2, "Constants.PhotoKey.Content should be 'description._content'")
        
        let descriptionKey = components.first!
        let contentKey = components.last!
        
        let description = photoDict[descriptionKey] as? [String: AnyObject]
        
        return description?[contentKey] as? String
    }
    
    class func dateFromDict(photoDict: [String: AnyObject]) -> NSDate {
        let unixTimestamp = photoDict[Constants.PhotoKey.DateUpload.rawValue] as! String
        return NSDate(timeIntervalSince1970: Double(unixTimestamp)!)
    }
}

extension Photo {
    func downloadImage(completion: ImageDownloadCompletion) {
        NetworkEngine.fetch(NSURL(string: imageURL)!, completion: completion)
    }
    
    var formattedDateUpload: String? {
        if let date = dateUpload {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "MMMM d, yyyy h:mm a"
            return  formatter.stringFromDate(date)
        }
        return nil
    }
}
