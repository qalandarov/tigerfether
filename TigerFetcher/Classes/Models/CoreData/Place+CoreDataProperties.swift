//
//  Place+CoreDataProperties.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Place {

    @NSManaged var placeID: String?
    @NSManaged var woeID: NSNumber?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    @NSManaged var placeURL: String?
    @NSManaged var placeType: String?
    @NSManaged var placeTypeID: NSNumber?
    @NSManaged var timezone: String?
    @NSManaged var content: String?
    @NSManaged var woeName: String?
    @NSManaged var photoCount: NSNumber?
    @NSManaged var photos: NSSet?

}
