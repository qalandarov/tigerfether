//
//  Place.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import CoreData


class Place: NSManagedObject {
    
    class func importPlaces(places: [[String: AnyObject]]) -> [Place]? {
        return places.flatMap { return place($0) } // convert and return non-nil Place objects
    }

    // Get Place from Dictionary (if exists by ID or create a new one)
    class func place(placeDict: [String: AnyObject]) -> Place? {
        
        if let placeID = placeDict[Constants.PlaceKey.PlaceID.rawValue] as? String
        {
            let place = placeByID(placeID) ?? emptyPlace()
            update(place, withDictionary: placeDict)
            
            return place
        }
        
        return nil
    }
    
    class func update(place: Place, withDictionary placeDict: [String: AnyObject]) {
        place.placeID      = placeDict[Constants.PlaceKey.PlaceID.rawValue]     as? String
        place.content      = placeDict[Constants.PlaceKey.Content.rawValue]     as? String
        place.latitude     = placeDict[Constants.PlaceKey.Latitude.rawValue]    as? Float
        place.longitude    = placeDict[Constants.PlaceKey.Longitude.rawValue]   as? Float
        place.photoCount   = Int(placeDict[Constants.PlaceKey.PhotoCount.rawValue] as! String)
        place.placeType    = placeDict[Constants.PlaceKey.PlaceType.rawValue]   as? String
        place.placeTypeID  = placeDict[Constants.PlaceKey.PlaceTypeID.rawValue] as? Int
        place.placeURL     = placeDict[Constants.PlaceKey.PlaceURL.rawValue]    as? String
        place.timezone     = placeDict[Constants.PlaceKey.TimeZone.rawValue]    as? String
        place.woeID        = placeDict[Constants.PlaceKey.WOEID.rawValue]       as? Int
        place.woeName      = placeDict[Constants.PlaceKey.WOEName.rawValue]     as? String
    }
    
    // MARK: - Helpers
    
    class func placeByID(placeID: String) -> Place? {
        assert(placeID.characters.count > 0, "PlaceID should not be empty!")
        
        let request = NSFetchRequest(entityName: "Place")
        request.predicate = NSPredicate(format: "placeID = %@", placeID)
        
        var place : Place?
        
        do {
            try place = DataBase.shared.context?.executeFetchRequest(request).first as? Place
        } catch {
            print("Error executing a fetch request: \(error)")
        }
        
        return place
    }
    
    class func emptyPlace() -> Place {
        return NSEntityDescription.insertNewObjectForEntityForName("Place", inManagedObjectContext: DataBase.shared.context!) as! Place
    }

}

extension Place {
    func downloadPhotos(completion: NetworkCompletion) {
        NetworkEngine.fetch(Flickr.URL.PlaceURL(place: self).url, completion: completion)
    }
}
