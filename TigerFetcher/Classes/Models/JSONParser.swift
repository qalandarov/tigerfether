//
//  JSONParser.swift
//  TigerFetcher
//
//  Created by Islam on 5/14/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation

class JSONParser {
    
    static func parsePlaces(data: NSData?) -> [Place]? {
        if let json = parse(data) {
            if let places = json["places"] as? [String: AnyObject] { // cast as dictionary
                if let placeArray = places["place"] as? [[String: AnyObject]] { // cast as array of dictionaries
                    return Place.importPlaces(placeArray)
                }
            }
        }
        
        return nil
    }
    
    static func parsePhotos(data: NSData?) -> [Photo]? {
        if let json = parse(data) {
            if let photos = json["photos"] as? [String: AnyObject] { // cast as dictionary
                if let photoArray = photos["photo"] as? [[String: AnyObject]] { // cast as array of dictionaries
                    return Photo.importPhotos(photoArray)
                }
            }
        }
        
        return nil
    }
    
    // MARK: - Parser
    
    private static func parse(data: NSData?) -> AnyObject? {
        do {
            if data != nil {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                return json
            }
        } catch {
            print("Error parsing json: \(error)")
        }
        
        return nil
    }
    
}