//
//  NetworkEngine.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation

typealias NetworkCompletion          = (NSData?, String?) -> Void
typealias PhotosCompletion           = ([Photo]?, String?) -> Void
typealias PlacesCompletion           = ([Place]?, String?) -> Void
typealias ImageDownloadCompletion    = (NSData?, String?) -> Void

class NetworkEngine {
    
    static func topPlaces(completion: PlacesCompletion) {
        fetch(Flickr.URL.TopPlaces.url) { (data, errorMsg) in
            if errorMsg != nil {
                completion(nil, errorMsg)
                return
            }
            
            let places  = JSONParser.parsePlaces(data)
            
            let error: String? = (places?.isEmpty == true ? "No results" : nil )
            
            completion(places, error)
        }
    }
    
    static func fetchRecentGeoPhotos(completion: PhotosCompletion) {
        fetch(Flickr.URL.RecentGeoPhotos.url) { (data, errorMsg) in
            if errorMsg != nil {
                completion(nil, errorMsg)
                return
            }
            
            let photos = JSONParser.parsePhotos(data)
            
            let error: String? = (photos?.isEmpty == true ? "No results" : nil)
            
            completion(photos, error)
        }
    }
    
    // MARK: - Helpers
    
    // Convinient func to process a URL and return on the main thread
    static func fetch(url: NSURL, completion: NetworkCompletion) {
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, _, error) in
            decrementNetworkActivity()
            
            dispatch_async(dispatch_get_main_queue(), {
                completion(data, error?.localizedDescription)
            })
        }
        
        incrementNetworkActivity()
        task.resume()
    }
    
    private static func incrementNetworkActivity() {
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notification.IncrementNetworkActivity.name, object: nil)
    }
    
    private static func decrementNetworkActivity() {
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.Notification.DecrementNetworkActivity.name, object: nil)
    }
    
}