//
//  Constants.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation

struct Constants {
    
    enum Notification: String {
        case IncrementNetworkActivity
        case DecrementNetworkActivity
        
        var name: String {
            return self.rawValue + "Notification"
        }
    }
    
    enum PlaceKey: String {
        case PlaceID        = "place_id"
        case Content        = "_content"
        case Latitude       = "latitude"
        case Longitude      = "longitude"
        case PhotoCount     = "photo_count"
        case PlaceType      = "place_type"
        case PlaceTypeID    = "place_type_id"
        case PlaceURL       = "place_url"
        case TimeZone       = "timezone"
        case WOEID          = "woeid"
        case WOEName        = "woe_name"
    }
    
    enum PhotoKey: String {
        case PhotoID        = "id"
        case PlaceID        = "place_id"
        case Content        = "description._content"
        case DateUpload     = "dateupload"
        case Farm           = "farm"
        case Latitude       = "latitude"
        case Longitude      = "longitude"
        case OriginalFormat = "originalformat"
        case OriginalSecret = "originalsecret"
        case OwnerName      = "ownername"
        case Secret         = "secret"
        case Server         = "server"
        case Tags           = "tags"
        case Title          = "title"
        case WOEID          = "woeid"
    }
    
}