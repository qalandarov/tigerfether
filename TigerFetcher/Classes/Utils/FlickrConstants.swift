//
//  Flickr.swift
//  TigerFetcher
//
//  Created by Islam on 5/14/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation

struct Flickr {
    
    private static let API_KEY = "c55f5a419863413f77af53764f86bd66"
    private static let PHOTOS_PER_PAGE = 25 // default = 250
    
    private static let Base = "https://api.flickr.com/services/rest/?api_key=\(API_KEY)&per_page=\(PHOTOS_PER_PAGE)&format=json&nojsoncallback=1&method=flickr."
    private static let Extras = "&extras=original_format,tags,description,geo,date_upload,owner_name"
    
    // Found these endpoints online (with their default values)
    enum URL {
        case TopPlaces
        case PlaceInfo(placeID: String)
        case RecentGeoPhotos
        case ImageURL(photo: Photo)
        case PlaceURL(place: Place)
        
        var path: String {
            
            switch self {
            case .TopPlaces:
                return Base + "places.getTopPlacesList&place_type_id=7"
                
            case .PlaceInfo(let placeID):
                return Base + "places.getInfo&place_id=\(placeID)"
                
            case .RecentGeoPhotos:
                return Base + "photos.search&license=1,2,4,7&has_geo=1" + Extras
                
            case .ImageURL(let photo):
                let format = FlickPhotoFormat.Large.rawValue
                return "https://farm\(photo.farm!).static.flickr.com/\(photo.server!)/\(photo.photoID!)_\(photo.secret!)_\(format).jpg"
                
            case .PlaceURL(let place):
                return Base + "photos.search&place_id=\(place.placeID!)" + Extras
            }
            
        }
        
        var url: NSURL {
            return NSURL(string: self.path)!
        }
    }
    
    // MARK: - Helpers
    
    enum FlickPhotoFormat: String {
        case Square = "s"
        case Large = "b"
        case Original = "o"
    }
    
}