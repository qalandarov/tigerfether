//
//  CoreDataTableViewController.swift
//  Contest
//
//  Created by Islam on 2/16/16.
//  Copyright © 2016 Home. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class CoreDataTableViewController : UITableViewController, NSFetchedResultsControllerDelegate {
    
    // MARK: - Instance variables
    
    var fetchedResultsController: NSFetchedResultsController? {
        didSet {
            fetchedResultsController!.delegate = self
            if title == nil && (navigationController == nil || navigationItem.title == nil) {
                title = fetchedResultsController!.fetchRequest.entity!.name
            }
            performFetch()
        }
    }
    
    // MARK: - Fetching
    
    func performFetch() {
        do {
            try fetchedResultsController!.performFetch()
        } catch let error as NSError {
            NSLog("%@", error.localizedDescription)
        }
        
        tableView.reloadData()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if fetchedResultsController?.sections?.count > 0 {
            let sectionInfo = fetchedResultsController!.sections![section] as NSFetchedResultsSectionInfo
            rows = sectionInfo.numberOfObjects
        }
        return rows
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        if let sections = fetchedResultsController?.sections {
            let sectionInfo = sections[section] as NSFetchedResultsSectionInfo
            return sectionInfo.name
        }
        return ""
    }
    
    override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return fetchedResultsController?.sectionForSectionIndexTitle(title, atIndex: index) ?? 0
    }
    
//    override func sectionIndexTitlesForTableView(tableView: UITableView!) -> [AnyObject]! {
//        return fetchedResultsController!.sectionIndexTitles
//    }
    
    // MARK: - NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        let indexSet = NSIndexSet(index: sectionIndex)
        switch type {
        case .Insert:
            tableView.insertSections(indexSet, withRowAnimation: UITableViewRowAnimation.Fade)
        case .Delete:
            tableView.deleteSections(indexSet, withRowAnimation: UITableViewRowAnimation.Fade)
        case .Update:
            break
        case .Move:
            break
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        case .Update:
            tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
}