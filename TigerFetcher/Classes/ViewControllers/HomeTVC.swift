//
//  HomeTVC.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class HomeTVC: UITableViewController {
    
    @IBOutlet weak var topPlaceImageView: UIImageView!
    @IBOutlet weak var recentPhotoImageView: UIImageView!
    
    var allRecentPhotos: [Photo]?
    
    var recentPhoto: Photo? {
        didSet { updateImageView(recentPhotoImageView, fromPhoto: recentPhoto!) }
    }
    
    var topPlace: Place? {
        didSet {
            // Using "unowned" because self won't be nil here
            topPlace?.downloadPhotos({ [unowned self] data, errorMsg in
                if let photos = JSONParser.parsePhotos(data) {
                    self.updateImageView(self.topPlaceImageView, fromPhoto: photos.first!)
                } else {
                    print("error: \(errorMsg ?? "Unexpected error")")
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // To remove the header (value 0 is ignored by the system)
        tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, 0.0001, 0.0001))
        
        makeNetworkCalls()
    }
    
    func makeNetworkCalls() {
        NetworkEngine.topPlaces { (places, errorMsg) in
            if errorMsg != nil {
                print("error: \(errorMsg!)")
                return
            }
            
            self.topPlace = places?.first
        }
        
        NetworkEngine.fetchRecentGeoPhotos { (photos, errorMsg) in
            if errorMsg != nil {
                print("error: \(errorMsg!)")
                return
            }
            
            self.allRecentPhotos = photos
            self.recentPhoto = photos?.first
        }
    }
    
    // MARK: - Helper
    
    func updateImageView(imageView: UIImageView, fromPhoto photo: Photo) {
        photo.downloadImage { (imageData, errorMsg) in
            if errorMsg != nil {
                print("error: \(errorMsg!)")
                return
            }
            
            imageView.image = UIImage(data: imageData!)
        }
    }
    
}
