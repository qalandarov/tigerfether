//
//  PhotoListTVC.swift
//  TigerFetcher
//
//  Created by Islam on 5/14/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import CoreData

class PhotoListTVC: CoreDataTableViewController {
    
    var place: Place? {
        didSet {
            if place?.photos?.count == 0 {
                place?.downloadPhotos({ (data, errorMsg) in
                    if errorMsg != nil {
                        print("error: \(errorMsg!)")
                        return
                    }
                    
                    JSONParser.parsePhotos(data)
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpFetchedRequestController()
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

        cell.textLabel?.text = photos(indexPath).title
        cell.detailTextLabel?.text = photos(indexPath).formattedDateUpload

        return cell
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let preview = segue.destinationViewController as? PreviewVC
            preview?.imageURL = NSURL(string: photos(indexPath).imageURL)
        }
    }
    
    // MARK: - Helpers
    
    func setUpFetchedRequestController() {
        fetchedResultsController = {
            let fetchRequest = NSFetchRequest(entityName: "Photo")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "dateUpload", ascending: false)]
            
            if place != nil {
                fetchRequest.predicate = NSPredicate(format: "place = %@", place!)
            }
            
            return NSFetchedResultsController(
                fetchRequest: fetchRequest,
                managedObjectContext: DataBase.shared.context!,
                sectionNameKeyPath: nil,
                cacheName: nil)
            }()
    }

}

private extension PhotoListTVC {
    func photos(indexPath: NSIndexPath) -> Photo {
        return fetchedResultsController!.objectAtIndexPath(indexPath) as! Photo
    }
}
