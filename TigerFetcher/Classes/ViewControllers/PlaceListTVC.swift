//
//  PlaceListTVC.swift
//  TigerFetcher
//
//  Created by Islam on 5/16/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import CoreData

class PlaceListTVC: CoreDataTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpFetchedRequestController()
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        cell.textLabel?.text = places(indexPath).content
        cell.detailTextLabel?.text = "\(places(indexPath).photoCount!) Photo(s)"
        
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let photoList = segue.destinationViewController as? PhotoListTVC
            photoList?.place = places(indexPath)
        }
    }
    
    // MARK: - Helpers
    
    func setUpFetchedRequestController() {
        fetchedResultsController = {
            let fetchRequest = NSFetchRequest(entityName: "Place")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "content", ascending: true)]
            
            return NSFetchedResultsController(
                fetchRequest: fetchRequest,
                managedObjectContext: DataBase.shared.context!,
                sectionNameKeyPath: nil,
                cacheName: nil)
            }()
    }

}

private extension PlaceListTVC {
    func places(indexPath: NSIndexPath) -> Place {
        return fetchedResultsController!.objectAtIndexPath(indexPath) as! Place
    }
}
