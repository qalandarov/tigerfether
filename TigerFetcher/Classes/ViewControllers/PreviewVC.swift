//
//  PreviewVC.swift
//  TigerFetcher
//
//  Created by Islam on 5/15/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class PreviewVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var imageURL: NSURL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NetworkEngine.fetch(imageURL) { (imageData, errorMsg) in
            if errorMsg != nil {
                self.exitWithError(errorMsg!)
                return
            }
            
            self.spinner.stopAnimating()
            self.setImage(imageData!)
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    // MARK: - Helpers
    
    func setImage(imageData: NSData) {
        let image = UIImage(data: imageData)!
        
        imageView.image = image
        imageView.sizeToFit()
        
        scrollView.contentSize = image.size
        scrollView.maximumZoomScale = 2
        
        scrollView.setNeedsLayout()
    }
    
    func exitWithError(errorMsg: String) {
        print("Error downloading image: \(errorMsg)")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
